import React from 'react';
import {
  Router,
  Scene
} from 'react-native-router-flux';

import CenaPrincipal from './components/CenaPrincipal';
import Sobre from './components/Sobre';
import Outros from './components/Outros';
import Resultado from './components/Resultado';

const Routes = () => (
  <Router>
    <Scene key='principal' component={CenaPrincipal} initial={true} title='Cara ou Coroa' />
    <Scene key='sobreJogo' component={Sobre} title='Sobre o Jogo' />
    <Scene key='outrosJogo' component={Outros} title='Mais Jogos' />
    <Scene key='resultado' component={Resultado} title='Resultado' />
  </Router>
);

export default Routes;
