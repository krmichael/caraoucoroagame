import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight
} from 'react-native';

import { Actions } from 'react-native-router-flux';

const caraCoroaLogo = require('../../img/logo.png');
const btnJogar = require('../../img/botao_jogar.png');
const sobre = require('../../img/sobre_jogo.png');
const outros = require('../../img/outros_jogos.png');

export default class CenaPrincipal extends Component {
  render() {
    const { container, content, btnjogar, rodape } = styles;

    return (
      <View style={container}>
        <View style={content}>
          <Image source={caraCoroaLogo}/>

          <TouchableHighlight onPress={() => { Actions.resultado(); }} style={btnjogar}>
            <Image source={btnJogar}/>
          </TouchableHighlight>
        </View>

        <View style={rodape}>
          <TouchableHighlight onPress={() => { Actions.sobreJogo(); }}>
            <Image source={sobre}/>
          </TouchableHighlight>

          <TouchableHighlight onPress={() => { Actions.outrosJogo(); }}>
            <Image source={outros}/>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#61BD8C'
  },
  content: {
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnjogar: {
    marginTop: 10
  },
  rodape: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
