import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet
} from 'react-native';

const cara = require('../../img/moeda_cara.png');
const coroa = require('../../img/moeda_coroa.png');

export default class Resultado extends Component {
  constructor(props) {
    super(props);

    this.state = { result: '' }
  }

  componentWillMount() {
    const numAleatorio = Math.floor(Math.random() * 2);
    let caraOucoroa = '';

    if(numAleatorio === 0) {
      caraOucoroa = 'cara'
    } else {
      caraOucoroa = 'coroa'
    }

    this.setState({ result: caraOucoroa });
  }
  
  render() {
    const { resultado } = styles;

    if(this.state.result === 'cara') {
      return (
        <View style={resultado}>
          <Image source={cara}/>
        </View>
      );
    }
    return (
      <View style={resultado}>
        <Image source={coroa}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  resultado: {
    flex: 1,
    backgroundColor: '#61BD8C',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
