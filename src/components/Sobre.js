import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

export default class Sobre extends Component {
  render() {
    const { container } = styles;
    
    return (
      <View style={container}>
        <Text>Informações sobre o Jogo</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#61BD8C',
    paddingTop: 70
  }
});
